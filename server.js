﻿require("rootpath")();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const basicAuth = require("utils/basic-auth");
const errorHandler = require("utils/error-handler");
const router = express.Router();

var argv = require("minimist")(process.argv.slice(2));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use basic HTTP auth to secure the api
app.use(basicAuth);

// api routes
app.use("/users", require("./users/users.controller"));

app.use(errorHandler);

// start express server
const port = 3000;
const server = app.listen(port, function() {
    console.log("Server listening on port " + port);
});
module.exports = server