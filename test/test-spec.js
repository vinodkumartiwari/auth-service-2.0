let UserService = require('../users/user.service');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Auth Services', () => {
    it('should show the error message on invalid credentials', (done) => {
        let user = {
            username: "MTN_user",
            password: "MTN281#^@*"
        }
        chai.request(server)
            .post('/users/authenticate')
            .send(user)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('Username or password is incorrect');
                done();
            });
    });

    it('should return user details on valid credentials ', (done) => {
        let user = {
            username: "MTN_user@test.com",
            password: "MTN281#^@*"
        }
        chai.request(server)
            .post('/users/authenticate')
            .send(user)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.eql({ "id": 1, "username": "MTN_user@test.com", "password": "MTN281#^@*", "firstName": "MTN", "lastName": "User" })
                done();
            });
    });
});